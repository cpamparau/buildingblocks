﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;

public class Click : MonoBehaviour, IInputClickHandler
{
    private bool rotate = false;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        Debug.Log(gameObject.name + " clicked.");
        rotate = !rotate;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (rotate)
        {
            gameObject.transform.Rotate(new Vector3 (150,300,60) * Time.deltaTime);
        }
    }
}
